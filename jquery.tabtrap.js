/**
* This is a jQuery plugin which will trap the tab focus
* inside a dom element like div.
*
* You must call clearTabFocus function if you dont need tabFocus
* in order to avoid the memory leak.
*
* @author arunpra
*/

(function( $ ) {
	$.fn.trapTabFocus = function() {
		var children = this.find('select, input, textarea, button, a').filter(':visible');
		var firstElem = children.first();
		var lastElem = children.last();
		firstElem.focus();
		
		// Handles shift on last element
		lastElem.on('keydown', function (e) {
			if ((e.which === 9 && !e.shiftKey)) {
				e.preventDefault();
				firstElem.focus();
			}        
		});
		
		// Handles shift + tab on first input
		firstElem.on('keydown', function (e) {
			if ((e.which === 9 && e.shiftKey)) {
				e.preventDefault();
				lastElem.focus();            
			}        
		});        
		return this;
	};
	
	$.fn.clearTabFocus = function() {
		var children = this.find('select, input, textarea, button, a').filter(':visible');
		var firstElem = children.first();
		var lastElem = children.last();
		// Remove the event handlers
		lastElem.off('keydown');
		firstElem.off('keydown');
		return this;
	};
}(jQuery));

